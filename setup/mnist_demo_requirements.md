Installing the requirements for the MNIST demo
=============================

This requires you to be working with linux, probably Kubuntu/Lubuntu/Xubuntu/Ubuntu 16.04 is necessary.

The accompanying file ``mnist_demo_requirements.sh`` should install everything automatically if you can't be bothered understanding what's going on.

Step 1
--------

Open up a terminal. We'll be using ``bash``.

If you'd like, do some reading about Anaconda and Conda environments. It's cool, and we're using it.

Step 2
--------

Install some necessary prerequisite packages by running in a terminal:

    sudo apt-get update
    sudo apt-get dist-upgrade
    sudo apt-get install build-essential
    sudo apt-get install libcupti-dev
    sudo apt-get install python3-pip
    sudo apt-get install git
    sudo apt-get install graphviz

Step 3
-------

Download the Conda installer:

    wget https://repo.continuum.io/archive/Anaconda3-4.3.1-Linux-x86_64.sh # choose a newer version if you like

Run it (don't forget to accept the license):

    mkdir $HOME/software
    bash Anaconda3-4.3.1-Linux-x86_64.sh -p $HOME/software/anaconda3

It will install Anaconda in a directory called ``software/anaconda3`` in your home directory.

Step 4
--------

Add the following line to the file ``.bashrc`` in your home folder (the ``.bashrc`` file should already exist) so that whenever you start a terminal, Anaconda will be available:

    export PATH=$HOME/software/anaconda3/bin:$PATH

For the current terminal session you will need to also run the command directly in the current terminal session so that you can continue with the further steps below:

    export PATH=$HOME/software/anaconda3/bin:$PATH

Step 5
-------

Create a conda environment. Let's call it ``deep``. It will contain tensorflow and keras and all the programs we need.

     conda create --name deep

Step 6
-------

Activate the ``deep`` environment:

    source activate deep

(note: in order to use this environment in different terminal sessions you will need to run this command before you can use it).

(note that the ``activate`` command is made available to us because of what we did in step 4).

Step 7
--------

Install into the ``deep`` environment the packages we need.

    conda install theano keras tensorflow opencv pillow spyder matplotlib
    pip install git+https://github.com/nlhepler/pydot.git

Note that ``conda`` and ``pip`` are different systems for installing Python packages but both work so that packages are only installed within the currently active conda environment (``deep``).

Note that we have not installed the GPU version of tensorflow. To do that we would need to install tensorflow via ``pip`` instead of ``conda``.

Step 8
-------

Load a Python interpreter (still within the ``deep`` environment):

    python

And ensure keras is installed by running the following commands. The commands will also download some datasets for future use:

    from keras.models import Sequential
    from keras.datasets import mnist
    from keras.datasets import cifar10
    Sequential()
    mnist_data = mnist.load_data()
    cifar_data = cifar10.load_data()

If those commands completed without error, we can say that Keras was successfully installed.

Note that generally instead of typing ``python``, a common thing to do is to start an IDE with a built-in interpreter like ``spyder``.

Step 9
-------

Go ahead with the tutorial.

Here are the related slides:

[https://docs.google.com/presentation/d/11nS8wIRBqiep9Cky1btAk-TyIMPeZrD3UViFGDx4-CU/edit?usp=sharing](https://docs.google.com/presentation/d/11nS8wIRBqiep9Cky1btAk-TyIMPeZrD3UViFGDx4-CU/edit?usp=sharing)

Here is the related worksheet:

[https://docs.google.com/document/d/1r23jl_CxHuhK54n0aZi2juaQvayZn1cb2TzwIOgJHc8/edit?usp=sharing](https://docs.google.com/document/d/1r23jl_CxHuhK54n0aZi2juaQvayZn1cb2TzwIOgJHc8/edit?usp=sharing)

You can find all the code from the tutorial in the current repository, which you can download by typing:

    git clone https://bitbucket.org/damienjadeduff/keras_maker_itu.git

