#!/bin/bash

export ANACONDA_PATH_PARENT=/opt
export ANACONDA_PATH=$ANACONDA_PATH_PARENT/anaconda3
export ANACONDA_INSTALLER=Anaconda3-4.3.1-Linux-x86_64.sh
export ENVNAME=deep

sudo apt-get -y update || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y dist-upgrade  || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y install build-essential  || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y install libcupti-dev || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y install python3-pip || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y install git || { echo "Failed on line $LINENO" ; exit 1; }
sudo apt-get -y install graphviz || { echo "Failed on line $LINENO" ; exit 1; }
# sudo apt-get -y install nodejs-legacy nodejs npm || { echo "Failed on line $LINENO" ; exit 1; }

cd $HOME

git clone https://bitbucket.org/damienjadeduff/keras_maker_itu.git || { echo "Failed on line $LINENO" ; exit 1; }

mkdir -p ~/tmp
cd ~/tmp  || { echo "Failed on line $LINENO" ; exit 1; }
if [ -f "$ANACONDA_INSTALLER" ]
then
    echo Already seem to have got the anaconda installer
    echo if installation fails run 
    echo rm $ANACONDA_INSTALLER
    echo rm -r $ANACONDA_PATH
    echo then trying again
else
    wget https://repo.continuum.io/archive/$ANACONDA_INSTALLER
fi

if [ -d "$ANACONDA_PATH" ]
then
    echo Already seem to have made the anaconda environment
    echo if installation fails after this, try running
    echo rm -r $ANACONDA_PATH
    echo then trying again
else
    sudo bash $ANACONDA_INSTALLER -b -p $ANACONDA_PATH  || { echo "Failed on line $LINENO" ; exit 1; }
    sudo chown -R $USER $ANACONDA_PATH
fi

if [ "`grep $ANACONDA_PATH ~/.bashrc`" ]
then
    echo .bashrc already setup
else
    echo >> ~/.bashrc
    echo export PATH=$ANACONDA_PATH/bin:\$PATH >> ~/.bashrc
fi

export PATH=$ANACONDA_PATH/bin:$PATH 
echo PATH: $PATH

yes | conda create --name $ENVNAME

source activate $ENVNAME  || { echo "Failed on line $LINENO" ; exit 1; }

yes | conda install theano keras tensorflow opencv pillow spyder matplotlib

pip install git+https://github.com/nlhepler/pydot.git

#cache
python -c "from keras.datasets import mnist;mnist.load_data()"
python -c "from keras.models import Sequential;Sequential()" || { echo "YOUR INSTALL DID NOT SUCCEED" ; exit 1 ; }
python -c "from keras.datasets import cifar10;cifar10.load_data()"

cd $HOME

git clone https://bitbucket.org/damienjadeduff/keras_maker_itu.git

echo DONE. YOU ARE DONE.

