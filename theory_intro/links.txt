The CIFAR-10 dataset in Keras: http://machinelearningmastery.com/object-recognition-convolutional-neural-networks-keras-deep-learning-library/
More datasets built in to Keras: https://keras.io/datasets/
Machine Learning with Python: https://www.coursera.org/learn/python-data-analysis/
Stanford CS231n Convolutional Neural Networks course: http://cs231n.stanford.edu/
Another good resource: http://machinelearningmastery.com/start-here/
MNIST net visualization: https://www.youtube.com/watch?v=3JQ3hYko51Y
Tensorflow playground: http://playground.tensorflow.org