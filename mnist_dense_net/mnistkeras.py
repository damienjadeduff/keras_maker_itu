import os
from keras.datasets import mnist
from keras.models import Sequential
from keras.layers.core import Dense, Dropout, Activation
from keras.optimizers import RMSprop
from keras.utils import np_utils
import matplotlib.pyplot as plt
import numpy
# Random seed for reproduction
seed = 7
numpy.random.seed(seed)

(X_train, y_train), (X_test, y_test) = mnist.load_data()
plt.imshow(X_train[1])
plt.show()

# Scaling value from 0-255 to 0-1
X_train = X_train.astype('float32')
X_test = X_test.astype('float32')
X_train /= 255
X_test /= 255


# Making labels one hot vectors
nb_classes=10
Y_train = np_utils.to_categorical(y_train, nb_classes)
Y = Y_train
Y_ev = np_utils.to_categorical(y_test, nb_classes)

# Reshaping input to one row per data
X = numpy.reshape(X_train,(60000,784))
X_ev = numpy.reshape(X_test,(10000,784))

# Creating a simple Neural Network Model
model = Sequential()
model.add(Dense(512, input_shape=(784,)))
model.add(Activation('relu'))
model.add(Dense(512))
model.add(Activation('relu'))
model.add(Dense(10)) # Ten output for classes
model.add(Activation('softmax')) # Softmax scores as results

# Compiling
model.compile(loss='categorical_crossentropy', optimizer='adam', metrics=['accuracy'])

# Visualize the Model
from keras.utils.visualize_util import plot
plot(model, to_file='model.png',show_shapes = True,show_layer_names = False)

# Fitting
model.fit(X, Y, nb_epoch=5, batch_size=1000,validation_data=(X_ev, Y_ev))

# Evaluation
scores = model.evaluate(X_ev, Y_ev)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))
