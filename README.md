Contents
========

**setup**: A script for installing the necessary software.

**theory_intro**: Some links.



**mnist_simplest**: No hidden layers example.

**mnist_dense_net**: One hidden layer example.

**mnist_demo_2layer_convolutional**: Two convolutional layers, with max pooling, dropout, one hidden dense layer.

