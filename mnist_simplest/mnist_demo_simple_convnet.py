#The name for this is actually logistic regression...
from keras.datasets import mnist
import matplotlib.pyplot as plt
from keras.models import Sequential
from keras.utils import np_utils
from keras.layers.core import Dense
from keras.layers import Conv2D, Flatten
import numpy as np

(X_train, y_train), (X_test, y_test) = mnist.load_data()

Y_train = np_utils.to_categorical(y_train, 10)
Y_test = np_utils.to_categorical(y_test, 10)

X_train_1ch = X_train.reshape(X_train.shape[0], 28, 28, 1)
X_test_1ch = X_test.reshape(X_test.shape[0], 28, 28, 1)
X_train_1ch = X_train_1ch.astype('float32') / 255
X_test_1ch = X_test_1ch.astype('float32') / 255


model = Sequential()
model.add(Conv2D(32,(3,3),activation='relu',input_shape=(28,28,1)))
model.add(Flatten())
model.add(Dense(512,activation="relu"))
model.add(Dense(10,activation="softmax"))


model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])

model.compile(loss='categorical_crossentropy', optimizer='sgd', metrics=['accuracy'])
history = model.fit(X_train_1ch, Y_train, nb_epoch=4, 
               batch_size=32,validation_data=(X_test_1ch, Y_test))


print()
print()

print("For first 5, image, label and prediction")

for ind in range(0,5):
    print()
    print("Image:")
    plt.imshow(X_test[ind])
    plt.show()
    print("Ground truth:")
    print(y_test[ind])
    predictions = model.predict(X_test_1ch[ind:ind+1,:])
    print("Their scores:")
    for num in range(0,9):
        print("",num,"-->",predictions[:,num])
    print("Prediction:",np.argmax(predictions))
    
print()
print("Test on full test dataset:")

print("A plot of test set accuracy against each epoch:")
plt.plot(history.history['val_acc'])
plt.show()

scores = model.evaluate(X_test_1ch, Y_test)
print("%s: %.2f%%" % (model.metrics_names[1], scores[1]*100))

from keras.models import Model
layer = Model(inputs=model.input,outputs=model.layers[0].output)
 



conv_images = layer.predict(X_train_1ch[5:6])
plt.imshow(conv_images[0,:,:,0],cmap="gray")
plt.show()
plt.imshow(conv_images[0,:,:,1],cmap="gray")
plt.show()
plt.imshow(conv_images[0,:,:,2],cmap="gray")
plt.show()

plt.imshow(conv_images[0,:,:,0:3])
plt.show()

